package com.rave.contactapp.viewmodel

import androidx.lifecycle.ViewModel
import com.rave.contactapp.model.Address
import com.rave.contactapp.model.Contact
import com.rave.contactapp.model.contactList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

/**
 * ViewModel containing state with current contacts data.
 *
 * @constructor Create empty Contact view model
 */
class ContactViewModel : ViewModel() {

    private val _state = MutableStateFlow(ContactState())
    val state get() = _state.asStateFlow()

    /**
     * Load state with default list of contacts.
     *
     */
    fun loadDefaults() {
        _state.update { state -> state.copy(contacts = contactList) }
    }

    /**
     * Add new contact to state.
     *
     * @param name
     * @param phones
     * @param emails
     * @param street
     * @param city
     * @param state
     * @param zipcode
     */
    @Suppress("LongParameterList")
    fun addContact(
        name: String,
        phones: List<String>,
        emails: List<String>,
        street: String,
        city: String,
        state: String,
        zipcode: String
    ) {
        val contact = Contact(
            id = _state.value.contacts.size,
            name = name,
            phones = phones,
            emails = emails,
            address = Address(
                streetAddress = street,
                city = city,
                state = state,
                zipcode = zipcode
            ),
            "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
        )
        val newContacts = _state.value.contacts + contact
        _state.update { state -> state.copy(contacts = newContacts) }
    }

    /**
     * Edit contact.
     *
     * @param id
     * @param name
     * @param phones
     * @param emails
     * @param street
     * @param city
     * @param state
     * @param zipcode
     * @param picture
     */
    @Suppress("LongParameterList")
    fun editContact(
        id: Int,
        name: String,
        phones: List<String>,
        emails: List<String>,
        street: String,
        city: String,
        state: String,
        zipcode: String,
        picture: String
    ) {
        var contacts = _state.value.contacts.toMutableList()
        val contact = Contact(
            id = id,
            name = name,
            phones = phones,
            emails = emails,
            address = Address(
                streetAddress = street,
                city = city,
                state = state,
                zipcode = zipcode
            ),
            pictureUrl = picture
        )
        contacts[id] = contact
        val newContacts = contacts.toList()
        _state.update { state -> state.copy(contacts = newContacts) }
    }
}
