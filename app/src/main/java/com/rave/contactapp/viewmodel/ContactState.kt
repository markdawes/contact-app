package com.rave.contactapp.viewmodel

import com.rave.contactapp.model.Contact

/**
 * State that contacts current contact data.
 *
 * @constructor Create empty Contact state
 */
data class ContactState(
    val contacts: List<Contact> = emptyList()
)
