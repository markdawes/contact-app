package com.rave.contactapp.model

/**
 * Contact class and default contacts.
 *
 * @property id
 * @property name
 * @property phones
 * @property emails
 * @property address
 * @property pictureUrl
 * @constructor Create empty Contact
 */
data class Contact(
    val id: Int,
    val name: String,
    val phones: List<String>,
    val emails: List<String>,
    val address: Address,
    val pictureUrl: String
)

val contactList = arrayListOf(
    Contact(
        id = 0,
        name = "Michaela Runnings",
        phones = listOf<String>("+1 202-918-2132"),
        emails = listOf<String>("mrunnings@gmail.com"),
        address = Address(
            streetAddress = "998 Willow Ave",
            city = "Willingboro",
            state = "NJ",
            zipcode = "08046"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1485290334039-a3c69043e517?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
    ),
    Contact(
        id = 1,
        name = "John Pestridge",
        phones = listOf<String>("+1 212-879-7201"),
        emails = listOf<String>("jpestridge@yahoo.com"),
        address = Address(
            streetAddress = "5 Briarwood Road",
            city = "Delaware",
            state = "OH",
            zipcode = "43015"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1542178243-bc20204b769f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTB8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 2,
        name = "Dan Spicer",
        phones = listOf<String>("+1 505-644-2932"),
        emails = listOf<String>("dannyboy@gmail.com"),
        address = Address(
            streetAddress = "8360 Sussex Ave",
            city = "Jacksonville",
            state = "NC",
            zipcode = "28540"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1595152772835-219674b2a8a6?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NDd8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 3,
        name = "Keanu Dester",
        phones = listOf<String>("+1 908-420-3558"),
        emails = listOf<String>("mrreeves@gmail.com"),
        address = Address(
            streetAddress = "479 8th Road",
            city = "Dorchester Center",
            state = "MA",
            zipcode = "02124"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1597528380214-aa94bde3fc32?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NTZ8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 4,
        name = "Anichu Patel",
        phones = listOf<String>("+1 346-885-2578"),
        emails = listOf<String>("apatel@hotmail.com"),
        address = Address(
            streetAddress = "46 Cemetary Lane",
            city = "Erie",
            state = "PA",
            zipcode = "16506"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1598641795816-a84ac9eac40c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NjJ8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 5,
        name = "Kienla Onso",
        phones = listOf<String>("+1 505-659-4205"),
        emails = listOf<String>("kienla467@yahoo.com"),
        address = Address(
            streetAddress = "7101 Mayfield Court",
            city = "Dyersburg",
            state = "TN",
            zipcode = "38024"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1566895733044-d2bdda8b6234?ixid=MXwxMjA3fDB8MHxzZWFyY2h8ODh8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 6,
        name = "Andra Matthews",
        phones = listOf<String>("+1 234-494-5848"),
        emails = listOf<String>("andramat@hotmail.com"),
        address = Address(
            streetAddress = "541 East Fairview St",
            city = "Teaneck",
            state = "NJ",
            zipcode = "07666"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1530577197743-7adf14294584?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NTV8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 7,
        name = "Georgia S.",
        phones = listOf<String>("+1 505-644-3844"),
        emails = listOf<String>("notthestate@gmail.com"),
        address = Address(
            streetAddress = "484 Sherman Court",
            city = "Indian Trail",
            state = "NC",
            zipcode = "28079"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1547212371-eb5e6a4b590c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTA3fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 8,
        name = "Matt Dengo",
        phones = listOf<String>("+1 505-340-8758"),
        emails = listOf<String>("mdengo@yahoo.com"),
        address = Address(
            streetAddress = "7625 Roosevelt Ave",
            city = "Louisville",
            state = "KY",
            zipcode = "40207"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1578176603894-57973e38890f?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTE0fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 9,
        name = "Marsha T.",
        phones = listOf<String>("+1 323-205-4914"),
        emails = listOf<String>("marshatop@gmail.com"),
        address = Address(
            streetAddress = "96 Durham Ave",
            city = "Coram",
            state = "NY",
            zipcode = "11727"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1605087880595-8cc6db61f3c6?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTI0fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 10,
        name = "Invshu Patel",
        phones = listOf<String>("+1 505-644-5031"),
        emails = listOf<String>("ipatel@hotmail.com"),
        address = Address(
            streetAddress = "637 South Kirkland St",
            city = "Xenia",
            state = "OH",
            zipcode = "45385"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1561820009-8bef03ebf8e5?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTM3fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 11,
        name = "Braylen Nathan",
        phones = listOf<String>("+1 212-599-1727"),
        emails = listOf<String>("bnathan@gmail.com"),
        address = Address(
            streetAddress = "7814 Lexington Street",
            city = "Salem",
            state = "MA",
            zipcode = "01970"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1605857840732-188f2f08cb31?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTQ5fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 12,
        name = "Jorge Andre",
        phones = listOf<String>("+1 207-636-4646"),
        emails = listOf<String>("jandre@yahoo.com"),
        address = Address(
            streetAddress = "20 Cypress Ave",
            city = "Addison",
            state = "IL",
            zipcode = "60101"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1517588632672-9758d6acba04?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTYyfHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 13,
        name = "Jamison Dana",
        phones = listOf<String>("+1 505-644-0333"),
        emails = listOf<String>("jdana@gmail.com"),
        address = Address(
            streetAddress = "8664 Foster St",
            city = "Bolingbrook",
            state = "IL",
            zipcode = "60440"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1513690277738-c9bc7eb2a992?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTY1fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 14,
        name = "Laura S.",
        phones = listOf<String>("+1 234-327-7311"),
        emails = listOf<String>("laurapower@yahoo.com"),
        address = Address(
            streetAddress = "7708 Brickell Dr",
            city = "Hartselle",
            state = "AL",
            zipcode = "35640"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1513708847802-f2fa846a7eaa?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTg1fHxwb3J0cmFpdHxlbnwwfDJ8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    ),
    Contact(
        id = 15,
        name = "Manilla Andrews",
        phones = listOf<String>("+1 208-438-1341"),
        emails = listOf<String>("manillaandrews@gmail.com"),
        address = Address(
            streetAddress = "854 N Glendale St",
            city = "Ottawa",
            state = "IL",
            zipcode = "61350"
        ),
        @Suppress("MaxLineLength")
        "https://images.unsplash.com/photo-1543123820-ac4a5f77da38?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NDh8fHBvcnRyYWl0fGVufDB8MnwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    )
)
