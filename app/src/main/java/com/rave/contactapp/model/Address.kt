package com.rave.contactapp.model

/**
 * Holds data for contact's address.
 *
 * @constructor Create empty Address
 */
data class Address(
    val streetAddress: String = "",
    val city: String = "",
    val state: String = "",
    val zipcode: String = ""
)
