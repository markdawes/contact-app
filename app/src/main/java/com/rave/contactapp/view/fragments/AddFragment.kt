package com.rave.contactapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.rave.contactapp.view.common.AppBar
import com.rave.contactapp.view.common.Email
import com.rave.contactapp.view.common.Field
import com.rave.contactapp.view.common.Form
import com.rave.contactapp.view.common.FormState
import com.rave.contactapp.view.common.Required
import com.rave.contactapp.view.ui.theme.ContactAppTheme
import com.rave.contactapp.viewmodel.ContactViewModel

/**
 * Screen for adding a contact.
 *
 * @constructor Create empty Add fragment
 */
class AddFragment : Fragment() {

    private val viewModel: ContactViewModel by activityViewModels()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ContactAppTheme {
                    Scaffold(topBar = {
                        AppBar(
                            title = "Add New Contact",
                            icon = Icons.Default.ArrowBack
                        ) {
                            findNavController().navigateUp()
                        }
                    }) {
                        Surface(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(it)
                        ) {
                            val state by remember { mutableStateOf(FormState()) }

                            Column {
                                Form(
                                    state = state,
                                    fields = listOf(
                                        Field(name = "name", label = "Contact Name", validators = listOf(Required())),
                                        Field(name = "phone", label = "Phone Number", validators = listOf(Required())),
                                        @Suppress("MaxLineLength")
                                        Field(name = "email", label = "Email Address", validators = listOf(Required(), Email())),
                                        @Suppress("MaxLineLength")
                                        Field(name = "street", label = "Street Address", validators = listOf(Required())),
                                        Field(name = "city", label = "City", validators = listOf(Required())),
                                        Field(name = "state", label = "State", validators = listOf(Required())),
                                        Field(name = "zipcode", label = "Zipcode", validators = listOf(Required()))
                                    )
                                )
                                Button(onClick = {
                                    if (state.validate()) {
                                        @Suppress("MagicNumber")
                                        viewModel.addContact(
                                            name = state.fields[0].text,
                                            phones = listOf(state.fields[1].text),
                                            emails = listOf(state.fields[2].text),
                                            street = state.fields[3].text,
                                            city = state.fields[4].text,
                                            state = state.fields[5].text,
                                            zipcode = state.fields[6].text
                                        )
                                        findNavController().navigateUp()
                                    }
                                }) {
                                    Text("Submit")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
