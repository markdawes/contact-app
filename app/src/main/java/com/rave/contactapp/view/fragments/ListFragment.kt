package com.rave.contactapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import coil.compose.AsyncImage
import com.rave.contactapp.R
import com.rave.contactapp.model.Contact
import com.rave.contactapp.view.common.AppBar
import com.rave.contactapp.view.ui.theme.ContactAppTheme
import com.rave.contactapp.viewmodel.ContactViewModel

/**
 * Screen for displaying contact list.
 *
 * @constructor Create empty List fragment
 */
class ListFragment : Fragment() {

    private val viewModel: ContactViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by viewModel.state.collectAsState()
                val navController = findNavController()
                ContactAppTheme {
                    ContactListScreen(contacts = state.contacts, navController)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ContactListScreen(contacts: List<Contact>, navController: NavController) {
    Scaffold(topBar = {
        AppBar(
            title = "Contact List",
            icon = Icons.Default.Home
        ) { }
    }) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = Color.LightGray
        ) {
            LazyColumn {
                items(contacts) { contact ->
                    ContactCard(contact = contact) {
                        val args: Bundle = bundleOf("contactId" to contact.id)
                        navController.navigate(R.id.action_listFragment_to_detailFragment, args)
                    }
                }
            }
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.BottomEnd
            ) {
                Button(
                    modifier = Modifier.padding(10.dp),
                    onClick = {
                        navController.navigate(R.id.action_listFragment_to_addFragment)
                    }
                ) {
                    Text("New\nContact")
                }
            }
        }
    }
}

@Composable
fun ContactCard(contact: Contact, clickAction: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(top = 8.dp, bottom = 4.dp, start = 16.dp, end = 16.dp)
            .fillMaxWidth()
            .wrapContentHeight(align = Alignment.Top)
            .clickable(onClick = { clickAction.invoke() }),
        elevation = CardDefaults.elevatedCardElevation(),
        colors = CardDefaults.cardColors(
            containerColor = Color.White
        )
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start
        ) {
            ContactPicture(contact.pictureUrl, 72.dp)
            ContactContent(contact.name, Alignment.Start)
        }
    }
}

@Composable
fun ContactPicture(pictureUrl: String, imageSize: Dp) {
    Card(
        shape = CircleShape,
        border = BorderStroke(
            width = 2.dp,
            color = Color.Gray
        ),
        modifier = Modifier
            .padding(16.dp)
            .size(imageSize),
        elevation = CardDefaults.elevatedCardElevation()
    ) {
        AsyncImage(
            model = pictureUrl,
            contentDescription = "Profile picture description"
        )
    }
}

@Composable
fun ContactContent(name: String, alignment: Alignment.Horizontal) {
    Column(
        modifier = Modifier.padding(8.dp),
        horizontalAlignment = alignment
    ) {
        Text(name, style = MaterialTheme.typography.headlineSmall)
    }
}
