package com.rave.contactapp.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rave.contactapp.R
import com.rave.contactapp.viewmodel.ContactViewModel

/**
 * Entry point for application.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : FragmentActivity() {
    @Suppress("LateinitUsage")
    private lateinit var navController: NavController
    private val contactViewModel by viewModels<ContactViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        contactViewModel.loadDefaults()

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }
}
