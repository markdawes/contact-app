package com.rave.contactapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.rave.contactapp.model.Contact
import com.rave.contactapp.view.common.AppBar
import com.rave.contactapp.view.common.Email
import com.rave.contactapp.view.common.Field
import com.rave.contactapp.view.common.Form
import com.rave.contactapp.view.common.FormState
import com.rave.contactapp.view.common.Required
import com.rave.contactapp.view.ui.theme.ContactAppTheme
import com.rave.contactapp.viewmodel.ContactViewModel

/**
 * Screen for editing contact details.
 *
 * @constructor Create empty Edit fragment
 */
class EditFragment : Fragment() {

    private val viewModel: ContactViewModel by activityViewModels()
    private val contactId by lazy { arguments?.getInt("contactId") }

    @OptIn(ExperimentalMaterial3Api::class)
    @Suppress("LongMethod")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by viewModel.state.collectAsState()
                val editContact = state.contacts.first { contact: Contact -> contactId == contact.id }
                ContactAppTheme {
                    Scaffold(topBar = {
                        AppBar(
                            title = "Edit Contact",
                            icon = Icons.Default.ArrowBack
                        ) {
                            findNavController().navigateUp()
                        }
                    }) {
                        Surface(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(it)
                        ) {
                            val state by remember { mutableStateOf(FormState()) }

                            Column {
                                Form(
                                    state = state,
                                    fields = listOf(
                                        @Suppress("MaxLineLength")
                                        Field(name = "name", label = "Contact Name", validators = listOf(Required()), editContact.name),
                                        @Suppress("MaxLineLength")
                                        Field(name = "phone", label = "Phone Number", validators = listOf(Required()), editContact.phones[0]),
                                        @Suppress("MaxLineLength")
                                        Field(name = "email", label = "Email Address", validators = listOf(Required(), Email()), editContact.emails[0]),
                                        @Suppress("MaxLineLength")
                                        Field(name = "street", label = "Street Address", validators = listOf(Required()), editContact.address.streetAddress),
                                        @Suppress("MaxLineLength")
                                        Field(name = "city", label = "City", validators = listOf(Required()), editContact.address.city),
                                        @Suppress("MaxLineLength")
                                        Field(name = "state", label = "State", validators = listOf(Required()), editContact.address.state),
                                        @Suppress("MaxLineLength")
                                        Field(name = "zipcode", label = "Zipcode", validators = listOf(Required()), editContact.address.zipcode)
                                    )
                                )
                                Button(onClick = {
                                    if (state.validate()) {
                                        @Suppress("MagicNumber")
                                        viewModel.editContact(
                                            id = editContact.id,
                                            name = state.fields[0].text,
                                            phones = listOf(state.fields[1].text),
                                            emails = listOf(state.fields[2].text),
                                            street = state.fields[3].text,
                                            city = state.fields[4].text,
                                            state = state.fields[5].text,
                                            zipcode = state.fields[6].text,
                                            picture = editContact.pictureUrl
                                        )
                                        findNavController().navigateUp()
                                    }
                                }) {
                                    Text("Submit")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
