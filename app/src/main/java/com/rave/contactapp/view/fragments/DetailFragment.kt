package com.rave.contactapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rave.contactapp.R
import com.rave.contactapp.model.Address
import com.rave.contactapp.model.Contact
import com.rave.contactapp.view.common.AppBar
import com.rave.contactapp.view.ui.theme.ContactAppTheme
import com.rave.contactapp.viewmodel.ContactViewModel

/**
 * Screen for displaying contact details.
 *
 * @constructor Create empty Detail fragment
 */
class DetailFragment : Fragment() {

    private val viewModel: ContactViewModel by activityViewModels()
    private val contactId by lazy { arguments?.getInt("contactId") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by viewModel.state.collectAsState()
                val displayedContact = state.contacts.first { contact -> contactId == contact.id }
                val navController = findNavController()
                ContactAppTheme {
                    ContactDetailsScreen(contact = displayedContact, navController)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ContactDetailsScreen(contact: Contact, navController: NavController) {
    Scaffold(topBar = {
        AppBar(
            title = "Contact Details",
            icon = Icons.Default.ArrowBack
        ) {
            navController.navigateUp()
        }
    }) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top
            ) {
                ContactPicture(contact.pictureUrl, 152.dp)
                ContactContent(
                    contact.name,
                    contact.address,
                    contact.phones,
                    contact.emails,
                    Alignment.CenterHorizontally
                )
            }
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.TopEnd
            ) {
                Button(
                    modifier = Modifier.padding(10.dp),
                    onClick = {
                        val args: Bundle = bundleOf("contactId" to contact.id)
                        navController.navigate(R.id.action_detailFragment_to_editFragment, args)
                    }
                ) {
                    Text("Edit\nContact")
                }
            }
        }
    }
}

@Composable
fun ContactContent(
    name: String,
    address: Address,
    phones: List<String>,
    emails: List<String>,
    alignment: Alignment.Horizontal
) {
    Column(
        modifier = Modifier
            .padding(vertical = 16.dp)
            .fillMaxHeight(),
        horizontalAlignment = alignment,
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(name, style = MaterialTheme.typography.headlineLarge)
        Text(
            text = "${address.streetAddress}\n${address.city}, ${address.state} ${address.zipcode}",
            style = MaterialTheme.typography.headlineSmall
        )
        LazyColumn {
            items(phones) { phone ->
                Text(phone, style = MaterialTheme.typography.headlineSmall)
            }
        }
        LazyColumn {
            items(emails) { email ->
                Text(email, style = MaterialTheme.typography.headlineSmall)
            }
        }
    }
}
